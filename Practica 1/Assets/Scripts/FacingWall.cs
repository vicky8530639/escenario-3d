using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FacingWall : MonoBehaviour
{
    private Rigidbody rb;
    public Animator anim;
    
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        //anim = GetComponent<Animator>();

    }
    void OnCollisionEnter(Collision collision)
    {
        if(gameObject.CompareTag("YourWallTag"))
        {
            rb.velocity = Vector3.zero;
            anim.SetTrigger("idle");
        }
    }
}
